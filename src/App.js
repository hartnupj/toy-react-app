import { HashRouter, Navigate, Route, Routes } from 'react-router-dom';
import Page1 from './Page1';
import Page2 from './Page2';
import Page3 from './Page3';
import './App.css';

const basename = ((process.env.PUBLIC_URL || '') + '/').replace(/\/\/$/, '/');
console.log('basename', basename);

function App()
{
  return (
    <>
      <div className="App">
        <p>A toy for demoing Google Analytics</p>
        <HashRouter>
          <Routes>
            <Route path="/page1" element={<Page1 />} />
            <Route path="/page2" element={<Page2 />} />
            <Route path="/page3" element={<Page3 />} />
            <Route path="*" element={<Navigate to="/page1" />} />
          </Routes>
        </HashRouter>
      </div>
    </>
  );
}

export default App;
