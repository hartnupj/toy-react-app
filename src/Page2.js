import { Link } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Continuing your financial journey</h1>
        <p>This is page 2</p>
        <Link to="/page3">Next</Link>
      </header>
    </div>
  );
}

export default App;
