import { Link } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Welcome to this financial journey</h1>
        <p>This is page 1</p>
        <Link to="/page2">Next</Link>
      </header>
    </div>
  );
}

export default App;
